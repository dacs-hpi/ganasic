#!/usr/bin/env python3

import sys
import os
import argparse
import numpy as np
from core import ganasic_utils
from multiprocessing import Pool


def main():

    version = '0.2.0'

    usage = """ganasic_matrix.py -i file.in -o file.out [options]

    Calculate the similarity matrix.

    1) a set of reads is simulated for every reference genome.
    2) the simulated reads are mapped against all reference genomes individually.
    3) the resulting tsv-files are analyzed to calculate the similarity matrix. 

    Input:
    (-i) text file with the path of the fasta reference sequences, one per line
    
    Output:
    (-o) output prefix for similarity matrix (.npy)

    See the provided LICENSE file or source code for license information.
    """

    parser = argparse.ArgumentParser(usage=usage)

    parser.add_argument('-i', '--input-file', type=str, dest='input_file', required=True,
                        help='Input file with path to the fasta reference sequences (one per line)')
    parser.add_argument('-o', '--output-prefix', type=str, dest='output_prefix', default="similarity_matrix",
                        help='Output prefix for similarity matrix. Default: similarity_matrix')

    parser.add_argument('-s', '--read-simulator', choices=["mason", "dwgsim"], type=str, dest='read_simulator',
                        default="dwgsim", help='Read simulation tool to use (dwgsim, mason). Default: dwgsim')
    parser.add_argument('-m', '--mapper', type=str, choices=["ganon", "kallisto"], dest='mapper',
                        default="ganon", help='Mapping method to use (ganon, kallisto). Default: ganon')

    parser.add_argument('-b', '--mapper-build-args', type=str, dest='mapper_build_args',
                        default="", help='Arguments for the building step of the mapper')
    parser.add_argument('-c', '--mapper-classify-args', type=str, dest='mapper_classify_args',
                        default="", help='Arguments for the classification step of the mapper')

    parser.add_argument('-l', '--read-length', type=int, dest='read_length',
                        default=100, help='Length of simulated reads. Default: 100')
    parser.add_argument('-n', '--read-number', type=int, dest='read_number', default=250000,
                        help='Number of simulated reads from each reference. Default: 250000')
    parser.add_argument('-a', '--read-simulator-args', type=str,
                        dest='read_simulator_args', default="", help='Arguments for the read simulator')

    parser.add_argument('-t', '--threads', type=int, default=1,
                        dest='threads', help='Number of threads to use. Default: 1')
    parser.add_argument('--temp', type=str, dest='temp', default='./temp',
                        help='Directory to store temporary files. Default: ./temp')
    parser.add_argument('--force-run', dest='force_run', default=False, action='store_true',
                        help='Force run of commands even if output files already exist')
    parser.add_argument('-v', '--version', action='version', version='version: %(prog)s ' +
                        version, help="Show program's version number and exit.")

    args = parser.parse_args()

    if len(sys.argv[1:]) == 0:
        parser.print_help()
        sys.exit(1)

    threads = args.threads
    output_path = args.temp + "/"
    if not os.path.exists(output_path):
        print("Creating temporary directory %s" % output_path)
        os.makedirs(output_path)

    fasta_files = [line.rstrip()
                   for line in open(args.input_file, "r").readlines()]

    n_files = len(fasta_files)
    sim_files = [output_path +
                 ganasic_utils.get_file_id(file) + '.fastq' for file in fasta_files]
    db_prefix = output_path + args.mapper + "_index"

    # Build/index for mapper
    if args.mapper == "ganon":
        ext_index = [".ibf", ".hibf"]
        extra_params = " -t " + str(threads)
    elif args.mapper == "kallisto":
        ext_index = [""]
        extra_params = " "
    if any([os.path.exists(db_prefix + e) for e in ext_index]) and not args.force_run:
        print("Index found: " + db_prefix + "|".join(ext_index))
    else:
        print("Building " + args.mapper +
              " index for " + str(n_files) + " files")
        if not ganasic_utils.run_mapper_build[args.mapper](db_prefix, args.input_file, args.mapper_build_args + extra_params):
            print("Failed to build index")
            sys.exit(1)

    # Simulate reads
    p = Pool(threads)
    res = []
    for i in range(n_files):
        if os.path.exists(sim_files[i]) and not args.force_run:
            print("Simulated reads found: " + sim_files[i])
        else:
            res.append(p.apply_async(ganasic_utils.run_simulator[args.read_simulator],
                                     (fasta_files[i],
                                      sim_files[i],
                                      args.read_length,
                                      args.read_number,
                                      args.read_simulator_args, )))
    for i, r in enumerate(res):
        ret = r.get()
        if not ret:
            print("Failed to simulate reads")
            sys.exit(1)
        else:
            print(str(i) + ") Simulated reads created: " + ret, flush=True)
    p.close()

    # Map reads
    p = Pool(threads)
    extra_params = ""
    if args.mapper == "kallisto":
        ext_classify = "/pseudoalignments.tsv"
        if not args.mapper_classify_args:
            extra_params += " --fragment-length 100 --sd 20 "
    elif args.mapper == "ganon":
        ext_classify = ".rep"
        if not args.mapper_classify_args:
            extra_params += " --rel-cutoff 0.25 "
    if extra_params:
        print("WARNING:" + extra_params + "is used with " +
              args.mapper + ", to change it provide a different value with -c")

    res = []
    for i in range(n_files):
        output_classification = output_path + \
            ganasic_utils.get_file_id(fasta_files[i]) + ext_classify
        if os.path.exists(output_classification) and os.stat(output_classification).st_size > 0 and not args.force_run:
            print("Mapping found: " + output_classification)
        else:
            res.append(p.apply_async(ganasic_utils.run_mapper_classify[args.mapper],
                                     (output_path,
                                      ganasic_utils.get_file_id(
                                          fasta_files[i]),
                                      sim_files[i],
                                      db_prefix,
                                      args.mapper_classify_args + extra_params, )))
    for i, r in enumerate(res):
        ret = r.get()
        if not ret:
            print("Failed to map reads")
            sys.exit(1)
        else:
            print(str(i) + ") Mapping created: " + ret, flush=True)
    p.close()

    # Get counts for each simulation and add to a matrix
    p = Pool(threads)
    mapped_matrix = []
    print("Counting mapped reads with " + args.mapper)
    if args.mapper == "ganon":
        ordered_names = [ganasic_utils.get_file_id(
            file) for file in fasta_files]
        for i in range(n_files):
            file_rep = ganasic_utils.get_file_id(fasta_files[i])
            print(str(i) + ") Counting " + file_rep)
            classification_output = output_path + file_rep + ".rep"
            mapped_matrix.append(p.apply(ganasic_utils.ganon_counts,
                                         (classification_output,
                                          ordered_names)))

    elif args.mapper == "kallisto":
        # Read aux contig counts to check in which sequences reads were mapped
        contig_index = []
        with open(db_prefix + ".contigs", "r") as file:
            for line in file:
                if line.rstrip():
                    contig_index.append(int(line.rstrip()))

        for i in range(n_files):
            classification_output = output_path + \
                ganasic_utils.get_file_id(fasta_files[i])
            mapped_matrix.append(p.apply(ganasic_utils.kallisto_counts,
                                         (classification_output,
                                          contig_index, )))

    p.close()
    p.join()

    mapped_matrix = np.array(mapped_matrix).T
    np.save(args.output_prefix + ".npy", mapped_matrix)
    print("Similarity matrix " + str(mapped_matrix.shape) +
          " generated and saved to file: " + args.output_prefix + ".npy")


if __name__ == '__main__':
    main()
